#Nội dung tìm hiểu
 * Environment Setup
    - virtual environment
    - system interpreter
 * Modules vs Packages
 * Local and Global Variables
 * Decision Making
 * File I/O
 * Exceptions
 * Classes + OOP
 * Multithreaded Programming
 * Logging
 * Schedule 
 * Regular Expressions
 * Jsonchema
 * PEP-8 


**Thông qua quá trình tìm hiểu các chủ đề trên thì mình rút ra được những nội dung như sau:**
 * Các bước tạo 1 môi trường ảo python và cách thay đổi system interpreter trên VSCode.
 * Phân biệt được giữa module với package khác nhau như thế nào. Biết cách tạo, sử dụng và lợi ích của việc sử dụng module và package.
 * Biết cách phân biệt giữa biến local và global, khi nào thì sử dụng biến local và khi nào sử dụng biến global
 * Hiểu cách được mở, đọc, ghi, xóa file và các mode khác nhau khi làm việc với file như: w,r,a,a+....
 * Hiểu được exception là gì, cú pháp để tạo exception và cách  tự định nghĩa 1 cái exception riêng cho mình
 * Phân biệt được class  với object, cách tạo class và object. Hiểu được các tính chất của lập trình hướng đối tượng như: tính đóng gói, kế thừa, đa hình
 * Hiểu được thread là gì và lợi ích của việc sử dụng thread trong chương trình. Cách viết  thread trong chương trình bằng  module Threading.
 * Biết logging là gì, lí do sử dụng logging, phân biệt các level của logging, cách config format cho logging.
 * Hiểu được schedule 1 task như thế nào và biết cách tạo ra schedule cho 1 task theo thời gian nhất định.
 * Nắm kiến thức cơ bản về jsonschema, các khái niệm về oneof, anyof, allof, if/then/else. Biết cách validate dữ liệu JSON dựa trên jsonschema bằng python.
 *  Biết cách clean code thông qua tìm hiểu PEP-8 sau cho dễ đọc và dễ hiểu. Sử dụng thư viện hỗ trợ để phát hiện những đoạn chưa clean bằng flake8.
 *  Nắm được cơ bản việc quản lý thread thông qua pool và cách config pool size như thế nào. Phân biệt được giữa threading, asyncio, multiprocessing.
 *  Nắm được cách viết 1 expression thông qua thư viện re trên python. Biết những expression hay dùng và ứng dụng của việc dùng expression trong thực tế.

  
***Chi tiết về code, syntax và sample trong từng thư mục sẽ có file markdown riêng cụ thể là file guide.md.***