#PEP-8
**PEP-8 là tập hợp các chỉ dẫn về định dạng code, phong cách lập trình được chia sẻ chung giữa các lập trình viên Python. PEP-8 được đề xuất bởi các chuyên gia trong "hội đồng" sáng lập ngôn ngữ lập trình Python. PEP-8 cung cấp nhiều chỉ dẫn để lập trình viên có thể viết "clean code" trong Python.**

####Các qui ước hay dùng:
 * **Indentation - Thụt lề, dòng**
    * Nguyên tắc 04 khoảng trắng: Sử dụng bốn khoảng trắng cho mỗi level của đoạn code. Điều này mang ý nghĩa cú pháp của Python. Nếu số lượng khoảng trắng không đủ, khi thực hiện chạy, chương trình sẽ báo lỗi.
    * Mỗi dòng nên có độ dài nhỏ hơn 79 ký tự.
  
 * **White-space -Khoảng trắng**
    * Đặt khoảng trắng trước và sau (bao xung quanh) các toán tử nhị phân, logic và phép gán:`=, +=, -=, ==, <, >, !=, <>, <=, >=, in, not in, is, is not, and, or, not`
    * Với các toán tử số học `+, -, *, /, //` lựa chọn bao xung quanh các toán tử bằng khoảng trắng (ở cả hai bên) hoặc không có khoảng trắng nào.
    * Trong các dòng lệnh có chứa dấu :, dòng lệnh tiếp theo là chuyển level của block code (cuối cùng của dòng chứa từ khóa def, class, if, while,...) thì phía trước và phía sau dấu `:` không có khoảng trắng.
    * Khi bạn muốn comment code, sau dấu `#` nên đặt ít nhất một khoảng trắng.
  
 * **Blank-line - Dòng trống**
    * Trong một file (module), phía trên và phía dưới (bao xung quanh) của phần định nghĩa function, class là 02 blank-line.
    * Với các function được định nghĩa trong một class thì bao xung quanh của function là 01 blank-line.
    * Dòng cuối cùng của một file/module là một dòng trống.
####Công cụ hỗ trợ
**Công cụ thường được mọi người dùng trên command-line có tên flake8, đây là công cụ để hỗ trợ các lập trình viên phát hiện các vi phạm là style. Để sử dụng được ta phải cài đặt bằng lệnh: `pip install flake8`**

```
Code example in file main.py:

import math
def add2number(a,b=0):
    # add 2 numbers
    return a + b

def minus2number(a,b):
    # minus 2 numbers
    return a - b




def is_prime_optimal(number):
    if number in [2, 3,5] :
        return True

    
    max_range = int(math.sqrt(number)) + 1
    for value in range(53,max_range,2):
        if number % value == 0:
            return False
    return True


class SubNumber:
    

    def check_number_v1(self):
        pass


    def check_number_v2(self):
        pass
---------------------------------------------------
Result when run: flake8 main.py
main.py:2:1: E302 expected 2 blank lines, found 0
main.py:2:17: E231 missing whitespace after ','
main.py:6:1: E302 expected 2 blank lines, found 1
main.py:6:19: E231 missing whitespace after ','
main.py:13:1: E303 too many blank lines (4)
main.py:14:23: E231 missing whitespace after ','
main.py:14:26: E203 whitespace before ':'
main.py:17:1: W293 blank line contains whitespace
main.py:18:5: E303 too many blank lines (2)
main.py:19:26: E231 missing whitespace after ','
main.py:19:36: E231 missing whitespace after ','
main.py:26:1: W293 blank line contains whitespace
main.py:28:5: E303 too many blank lines (2)
main.py:32:5: E303 too many blank lines (2)
main.py:33:13: W292 no newline at end of file

---------------------------------------------------
Correct Code:
import math


def add2number(a, b=0):
    # add 2 numbers
    return a + b


def minus2number(a, b):
    # minus 2 numbers
    return a - b


def is_prime_optimal(number):
    if number in [2, 3, 5]:
        return True
    max_range = int(math.sqrt(number)) + 1
    for value in range(53, max_range, 2):
        if number % value == 0:
            return False
    return True


class SubNumber:
    def check_number_v1(self):
        pass

    def check_number_v2(self):
        pass

```