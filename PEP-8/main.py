import math


def add2number(a, b=0):
    # add 2 numbers
    return a + b


def minus2number(a, b):
    # minus 2 numbers
    return a - b


def is_prime_optimal(number):
    if number in [2, 3, 5]:
        return True
    max_range = int(math.sqrt(number)) + 1
    for value in range(53, max_range, 2):
        if number % value == 0:
            return False
    return True


class SubNumber:
    def check_number_v1(self):
        pass

    def check_number_v2(self):
        pass
