#Logging
**Logging là module tiêu chuẩn (được pack kèm bộ cài mặc định) do cộng đồng Python phát triển, dễ dàng sử dụng và cực kỳ linh hoạt. Logging cung cấp cho lập trình viên những tiện ích như:**
 * Phân chia level cho các thông báo lỗi, cho phép tuỳ chọn mức độ nghiêm trọng của các thông báo, cho phép hoặc không cho phép hiển thị một thông báo đã được phân loại.
 * Cho phép cấu hình output của thông báo lỗi là trên console hoặc file hoặc nguồn khác.
####Các level được liệt kê có độ nghiêm trọng lớn dần:
 * DEBUG: **Thông tin chi tiết, thường là thông tin để tìm lỗi.**
 * INFO: **Thông báo thông thường, các thông tin in ra khi chương trình chạy theo đúng kịch bản.**
 * WARNING: **Thông báo khi nghi vấn bất thường hoặc lỗi có thể xảy ra, tuy nhiên chương trình vẫn có thể hoạt động.**
 * ERROR: **Lỗi, chương trình có thể không hoạt động được một số chức năng hoặc nhiệm vụ nào đó, thường thì nên dùng ghi bắt được Exception.**
 * CRITICAL: **Lỗi, chương trình gặp lỗi nghiêm trọng không thể giải quyết được và bắt buộc phải dừng lại.**

```
Code example:
import  logging
logging.warning('cảnh báo') # sẽ in ra warning trên console
logging.debug('debug code') # không in gì cả

Result:
WARNING:root:cảnh báo
```
****
**Do WARNING có level cao hơn so với DEBUG vì vậy cái log của DEBUG sẽ không được in ra. Vì vậy ta cần thêm `logging.basicConfig(level=logging.DEBUG) vào đoạn code trên.
`**
```
Code example:
import  logging
logging.basicConfig(level=logging.DEBUG) 
logging.warning('cảnh báo')
logging.debug('debug code') 

Result:
WARNING:root:cảnh báo
DEBUG:root:debug code
```
**Để lưu log vào trong file ta dùng lệnh sau:`logging.basicConfig(filename='app.log',level=logging.DEBUG,filemode='w')
`**

**Format nội dung và thêm các thông tin chi tiết về log:**
```
Code example:
import logging
logging.basicConfig(level=logging.DEBUG, filename='app.log', filemode='w', format = (
                                                    '%(levelname)s:\t'
                                                    '%(filename)s:'
                                                    '%(funcName)s():'
                                                    '%(lineno)d\t'
                                                    '%(message)s'
                                                )
                    )
logging.warning('something wrong') 
logging.debug('debug code')  
logging.info('Run completed')  

Result in file app.log
WARNING:	main.py:<module>():10	something wrong
DEBUG:	main.py:<module>():11	debug code
INFO:	main.py:<module>():12	Run completed
```
**Việc sử dụng logger cơ bản chỉ cho phép chúng ta in log ra một nguồn tại một thời điểm, trong ví dụ trên, khi đặt filename='app.log' thì console sẽ không in nội dung log ra nữa. Một cách cho phép thực hiện việc in log ra nhiều nguồn khác nhau đó là sử dụng `handles`, tham khảo ví dụ in log ra `FileHandler` sau:**
```
Code example:

import logging
# create the logging instance for logging to file only
logger = logging.getLogger('SmartfileTest') 
#Set level
logger.setLevel(logging.DEBUG)
file_logger = logging.FileHandler('smartfile_test.log')
NEW_FORMAT = '[%(asctime)s] - [%(levelname)s] - %(message)s'
file_logger_format = logging.Formatter(NEW_FORMAT)
# tell the handler to use the above format
file_logger.setFormatter(file_logger_format) 
# finally, add the handler to the base logger
logger.addHandler(file_logger)
#use to print the logs
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
logging.getLogger('SmartfileTest').addHandler(console)
# log some stuff!
logger.debug("This is a debug message!") 
logger.info("This is an info message!")
logger.warning("This is a warning message!")

Result on console:
This is a debug message!
This is an info message!
This is a warning message!

Result in file smartfile_test.log:
[2022-01-06 09:55:44,264] - [DEBUG] - This is a debug message!
[2022-01-06 09:55:44,264] - [INFO] - This is an info message!
[2022-01-06 09:55:44,264] - [WARNING] - This is a warning message!

```