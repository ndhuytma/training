import logging

logger = logging.getLogger('SmartfileTest') # create the logging instance for logging to file only

logger.setLevel(logging.DEBUG)#Set level

file_logger = logging.FileHandler('smartfile_test.log')
NEW_FORMAT = '[%(asctime)s] - [%(levelname)s] - %(message)s'
file_logger_format = logging.Formatter(NEW_FORMAT)

file_logger.setFormatter(file_logger_format) # tell the handler to use the above format


logger.addHandler(file_logger) # finally, add the handler to the base logger


console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
logging.getLogger('SmartfileTest').addHandler(console)

logger.debug("This is a debug message!") # log some stuff!
logger.info("This is an info message!")
logger.warning("This is a warning message!")