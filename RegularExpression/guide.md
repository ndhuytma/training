#Regular Expression 
**Regular Expression hay còn gọi là Biểu thức chính quy là một đoạn các ký tự đặc biệt theo những khuôn mẫu (pattern) nhất định, đại diện cho chuỗi hoặc một tập các chuỗi. Regular Expression trong Python được thể hiện qua module `re`, nên việc đầu tiên khi các bạn muốn sử dụng Regular Expression thì cần phải `import re` vào chương trình.**
**Một số đoạn regex hay dùng:**
```
Check email:
'^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})*$'

Check URL:
# Bao gồm Http(s) Protocol
'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#()?&//=]*)'
# Có hoặc không có Http(s) Protocol đều được
'(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)'

Check phone number:
'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$'

Check username:
// Gồm chuỗi và số từ 8 đến 50 ký tự
'^[a-z0-9_-]{8,50}$'

Check password:
// Bao gồm cả chữ hoa, chữ thường, số, ký tự đặc biệt và ít nhất 10 kỹ tự
'(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"<<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{10,}' 
```
**Ứng dụng của regular expression trong thực tế:**
 * Kiểm tra input của người dùng nhập vào có hợp lệ trên các website
 * Loại bỏ những nội dung không cần thiết trong dữ liệu thu thập về
 * Kiểm tra dữ liệu nhận vào có hợp lệ không   

```
Code example:
import re
pattern='^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$'
num='+84349360265'
rs=re.match(pattern,num)
if rs:
    print(f'Phone number: {num} is valid')
else:
    print(f'Phone number: {num} is not valid')

Result:
Phone number: +84349360265 is valid
```