from jsonschema import validate


def checkValid(data):
    try:
        validate(instance=data, schema=schema)
        print(f"Data: {data} is valid")
    except Exception:
        print(f"Data: {data} is not valid")


schema = {
    "type": "object",
    "properties": {
        "street_address": {
            "type": "string"
        },
        "province": {
            "default": "HCM",
            "enum": ["HN", "HCM"]
        }
    },
    "if": {
        "properties": {"province": {"const": "HCM"}}
    },
    "then": {
        "properties": {"postal_code": {"pattern": "[0-9]{5}(-[0-9]{4})?"}}
    },
    "else": {
        "properties": {"postal_code": {"pattern": "[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"}}
    }
}
data1 = {
    "street_address": "61/6 Kha Van Can Thu Duc",
    "province": "HCM",
    "postal_code": "90800"
}
data2 = {
    "street_address": "30 Hai Ba Trung Hoan Kiem",
    "province": "HN",
    "postal_code": "10000"
}
def main():
    checkValid(data1)
    checkValid(data2)

if __name__ == '__main__':
    main()