#Json schema
**Json schema là tiêu chuẩn của tài liệu Json mô tả cấu trúc và các yêu cầu của dữ liệu Jso n của bạn. Json schema dùng để validate dữ liệu Json.**
####Schema composition and conditionally
 * **AllOf**: dữ liệu Json phải thỏa mãn điều kiện tất cả các subschemas.
  ```
    {
        "allOf": [
            { "type": "string" },
            { "maxLength": 5 }
        ]
    }
    "huy": correct
    "duchuy": wrong
 ```
 * **AnyOf**: dữ liệu Json thỏa mãn điều kiện bất cứ cái subschemas nào.
```
    {
        "anyOf": [
            { "type": "string", "maxLength": 5 },
            { "type": "number", "minimum": 0 }
        ]
    }
    "huy": correct
    "duchuy": wrong
    10: correct
    -1: wrong
 ```
 * **OneOf**: dữ liệu Json chỉ thỏa mãn điều kiện đúng duy nhất 1 cái subschemas.
```
    {
        "oneOf": [
            { "type": "number", "multipleOf": 5 },
            { "type": "number", "multipleOf": 3 }
        ]
    }
    9: correct
    10: correct
    15: wrong
    4:  wrong
 ```
 * **If/then/else**: nếu một trường dữ liệu Json nào đó thỏa điều kiện if thì kiểm tra tiếp trường dữ liệu khác có thỏa trong then không ngược lại thì kiểm tra dữ liệu có thỏa điều kiện trong else không.

```
{
  "type": "object",
  "properties": {
    "street_address": {
      "type": "string"
    },
    "country": {
      "default": "HCM",
      "enum": ["HN", "HCM"]
    }
  },
  "if": {
    "properties": { "country": { "const": "HCM" } }
  },
  "then": {
    "properties": { "postal_code": { "pattern": "[0-9]{5}(-[0-9]{4})?" } }
  },
  "else": {
    "properties": { "postal_code": { "pattern": "[A-Z][0-9][A-Z] [0-9][A-Z][0-9]" } }
  }
}
Correct:
{
  "street_address": "61/6 Kha Van Can Thu Duc",
  "country": "HCM",
  "postal_code": "98000"
}
Wrong:
{
  "street_address": "30 Hai Ba Trung",
  "country": "HN",
  "postal_code": "10000"
}
```
```
Apply code python:
from jsonschema import validate


def checkValid(data):
    try:
        validate(instance=data, schema=schema)
        print(f"Data: {data} is valid")
    except Exception:
        print(f"Data: {data} is not valid")


schema = {
    "type": "object",
    "properties": {
        "street_address": {
            "type": "string"
        },
        "province": {
            "default": "HCM",
            "enum": ["HN", "HCM"]
        }
    },
    "if": {
        "properties": {"province": {"const": "HCM"}}
    },
    "then": {
        "properties": {"postal_code": {"pattern": "[0-9]{5}(-[0-9]{4})?"}}
    },
    "else": {
        "properties": {"postal_code": {"pattern": "[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"}}
    }
}
data1 = {
    "street_address": "61/6 Kha Van Can Thu Duc",
    "province": "HCM",
    "postal_code": "90800"
}
data2 = {
    "street_address": "30 Hai Ba Trung Hoan Kiem",
    "province": "HN",
    "postal_code": "10000"
}
def main():
    checkValid(data1)
    checkValid(data2)

if __name__ == '__main__':
    main()

Result:
Data: {'street_address': '61/6 Kha Van Can Thu Duc', 'country': 'HCM', 'postal_code': '90800'} is valid
Data: {'street_address': '30 Hai Ba Trung Hoan Kiem', 'country': 'HN', 'postal_code': '10000'} is not valid
```